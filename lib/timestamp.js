'use babel';

import { CompositeDisposable } from 'atom';

export default {

  activate(state) {
    atom.commands.add('atom-workspace', {
      'timestamp:stamp': () => this.stamp()
    });
  },

  stamp() {
    const d = new Date();
		
		function pad(number) {
			if (number < 10) {
				return '0' + number;
			}
			return number;
		}
		
		let formattedDate =
			`${d.getFullYear()}-${pad(d.getMonth() + 1)}-${pad(d.getDate())}, ${pad(d.getHours())}:${pad(d.getMinutes())}`;
		
		let user = 'GRECI';
		
		let finalText = `// ${user} [${formattedDate}]:: `;
		
		let editor;
		if (editor = atom.workspace.getActivePaneItem()) {
			editor.insertText(finalText);
		}
  }

};